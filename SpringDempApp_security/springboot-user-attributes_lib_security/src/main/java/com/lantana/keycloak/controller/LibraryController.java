package com.lantana.keycloak.controller;
//package com.lantana.keycloak.config;

import com.lantana.keycloak.repository.BookRepository;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.jetty.core.AbstractKeycloakJettyAuthenticator.KeycloakAuthentication;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.springframework.beans.factory.annotation.Autowired;
import org.keycloak.KeycloakPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LibraryController {

	private final HttpServletRequest request;
	private final BookRepository bookRepository;

	@Autowired
	public LibraryController(HttpServletRequest request, BookRepository bookRepository) {
		this.request = request;
		this.bookRepository = bookRepository;
	}

	@GetMapping(value = "/")
	public String getHome() {
		return "index";
	}

	@GetMapping(value = "/books")
	public String getBooks(Model model) {
		configCommonAttributes(model);
		model.addAttribute("books", bookRepository.readAll());
		return "books";
	}

	@GetMapping(value = "/manager")
	public String getManager(Model model) {
		configCommonAttributes(model);
		model.addAttribute("books", bookRepository.readAll());
		return "manager";
	}

	@GetMapping(value = "/logout")
	public String logout() throws ServletException {
		//request.logout();
		//return "redirect: https://ssobeta.pmjay.gov.in/auth/realms/pmjay/protocol/openid-connect/logout?redirect_uri=http%3A%2F%2Flocalhost%3A8080%2F";
		//return "redirect:/";

		HttpSession session = getSession();
		session.invalidate();
		return "redirect:https://ssobeta.pmjay.gov.in/auth/realms/pmjay/protocol/openid-connect/logout?redirect_uri=http://localhost:8081/";
	}

	@GetMapping(value = "/softLogout")
	public String softLogout() throws ServletException, ClientProtocolException, IOException{
		
		
		// you can also extract values from the security context like this
		/*
		KeycloakSecurityContext context = getKeycloakSecurityContext();
		String access_token_2 = context.getIdTokenString();
		String user_id_String_2 = context.getIdToken().getSubject();
		String session_state_String_2 = context.getIdToken().getSessionState(); */
		
		
		
		KeycloakPrincipal<KeycloakSecurityContext> principal = getPrincipal();
		if (principal==null) {
			throw new RuntimeException("no principle found");
		}
		
		
		//String user_idString = principal.getName();
		//uid present in subject
		
		String user_idString = principal.getKeycloakSecurityContext().getIdToken().getSubject();
		String access_tokenString = principal.getKeycloakSecurityContext().getTokenString();
		String session_state = principal.getKeycloakSecurityContext().getToken().getSessionState();
		
		//String session_stateString = principal.getKeycloakSecurityContext().getIdToken().getSessionState();
		//above could be used if unable to get session_state from earlier line
		
		String urlString = "https://ssobeta.pmjay.gov.in/auth/realms/pmjay/api/logout/library/"+user_idString+"/"+session_state; 
		String bearer_string = "Bearer "+access_tokenString;		
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpDelete httpDelete = new HttpDelete(urlString);
		//httpDelete.setHeader("Content-type", "application/json");
		httpDelete.setHeader("Authorization", bearer_string);
		
		HttpResponse response = httpClient.execute(httpDelete);
		int status = response.getStatusLine().getStatusCode(); 
		System.out.println(status);
		
		if(!(status >=200 || status <=300)) {
			throw new RuntimeException("failed to end session");
		}
		
		HttpSession session = getSession();
		session.invalidate();
		
		return "redirect:/";	
	}
	
	private void configCommonAttributes(Model model) {
		model.addAttribute("name", getKeycloakSecurityContext().getIdToken().getGivenName());
	}

	private KeycloakSecurityContext getKeycloakSecurityContext() {
		return (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
	}

	// in security adapter this gives the token.keycloakAuthenticationToken
	// the way to get principal is bit different, shown below
	private KeycloakPrincipal<KeycloakSecurityContext> getPrincipal(){
	    KeycloakAuthenticationToken token = (KeycloakAuthenticationToken)request.getUserPrincipal();
	    KeycloakPrincipal<KeycloakSecurityContext> principal = (KeycloakPrincipal<KeycloakSecurityContext>)token.getPrincipal();
		return principal;
	}
	private HttpSession  getSession() {
		return request.getSession();
	}
}
